# Was ist der Diaspora-Optimizer?
Der Optimizer fügt neue Features zu Diaspora* hinzu. Nicht mehr und nicht weniger

# Wie installiere ich ihn?
Du kannst ihn über die Stores von Chrome oder FireFox installieren. Die dritte Option wäre es den Quellcode herunterzuladen und per Sideloading zu betreiben. Macht dann Sinn, wenn Du selber dran rumprogrammieren möchtest. Aber dann brauch ich Dir sicher nicht erklären, wie Du es installierst :)

# Warum eine Extension und nicht direkt an Diaspora* mitarbeiten?
Hauptsächlich aus drei Gründen:
1. Durch den G+ - Optimizer habe ich einige Erfahrungen mit der Entwicklung von Extensions gesammelt und fühle mich hier in meiner "Wohlfühlzone". Den Diaspora* - Code hingegen kenne ich (noch) nicht gut genug um da rumzufummeln
2. Als Extension lassen sich Features besser testen. Selbst wenn ich so richtig Mist baue, braucht man die Extension nur deaktivieren und alles geht wieder :)
3. Einige der Features möchte vermutlich nicht jeder. Bei einer Extension kann jeder selbst entscheiden. Im Core muss das zwingend von der Mehrheit getragen werden

# Ich habe einen Fehler gefunden
Ok. Trag ihn bei den "Issues" ein

# Ich möchte dringend dieses Feature
Ok. Trags bei den Issues ein. Aber denk dran: Ich mach das in meiner Freizeit. Also kein 24/7 Support oder so

## Dann müssen halt mehr mitarbeiten
Stimmt :) Sag jedem Bescheid, der sich mit JavaScript auskennt. Darum ist es ja Open Source.

# Ich vermisse meine Sprache
Ich spreche selbst nur Deutsch und Englisch (gerade gut genug). Andere Sprachen kannst Du aber selbst gerne hinzufügen. Andere User werden sich freuen

# Was **genau** macht denn nun der Optimizer
Schau das bitte in den Stores nach. Das ändert sich naturgemäß ständig

# Wie konfiguriere ich den Optimizer?
In deinem Pod ist unter "Einstellungen" ein neuer Menüpunkt "Optimizer" (mit der roten Glocke)

# Kann ich Dich kontaktieren?
Klar. ~~Über Google+~~ (_just kidding_) bei Diaspora: stammtischphilosoph@pluspora.com
Aber bitte nicht für Feature Requests oder Bugreports. Ich vergess das sonst einfach. Nutz lieber die Issues

