# What is the Diaspora-Optimizer?
The Optimizer adds features to Diaspora*. Simple as that.

# How to install?
You can install it using the chrome webstore or the Firefox store. Another option is to download the code and sideload it. I would prefer the first option, though if you are not wanting to modify the code. (And if you do, you most likely don't need a manual for installing an extension :) )

# Why not contribute to Diaspora* directly instead of creating an extension?
There are three main reasons:
1. Writing extensions is something I have some experience with because of the Google+ - Optimizer. I simply don't know the Diaspora-Code and structure good enough to contribute (yet)
2. Even if this gets into the Diaspora* - Code sometime the extension is a good way to test those features before.
3. Some features might not be wanted by everybody. An extension can simply be disabled if you don't like it. If it's in the core that can't be done that easily

# I found an issue
Fine. Use the "issues" - Tab on gitlab

# I have a feature request
Fine. Use the "issues" - Tab on gitlab. But please be aware that I do this on my spare time. Don't expect 24/7 response times

## Then we need more people coding here
I agree :) Tell everybody to help. This is open source after all :)

## What about my language?
I only speak german (mother's tongue) and english (just good enough). Feel free to add your own language. You only need to add a readme and translate those files under _locales.


# What **exactly** are the features?
Please check that in the Chrome/Firefox - store as this changes over time.

# How do I configure the Optimizer?
You got a new topic in your settings of your pod called "Optimizer" (with the red bell)

# Can I contact you directly?
Sure. ~~In Google+~~ (_just kidding_) in Diaspora: stammtischphilosoph@pluspora.com
Please not for feature requests or bugs. I simply will forget that. Use the issues instead

