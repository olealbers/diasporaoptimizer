var self=this;
var oldUrl=window.location.href;
var forEach = Array.prototype.forEach;
this.Log=new doptLog();
this.Modules=[];

$(document).ready(function () {
    DomCheckSetup();
    LoadModules();
});

$(document).on('click','#optimizer_nav', function() {
    $($('#settings_nav').find(".active")[0]).removeClass('active');
    $('#optimizer_nav').addClass('active');

    LoadSetupPage();
});

function DrawIcon(size) {
    if (!size) size=20;
    var lblBellTitle=L("bellTitle");
    var icon= '<svg  aria-hidden="true" width=[SIZE]px height=[SIZE]px data-prefix="fas" data-icon="concierge-bell" class="svg-inline--fa fa-concierge-bell fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="#a80000" d="M288 130.54V112h16c8.84 0 16-7.16 16-16V80c0-8.84-7.16-16-16-16h-96c-8.84 0-16 7.16-16 16v16c0 8.84 7.16 16 16 16h16v18.54C115.49 146.11 32 239.18 32 352h448c0-112.82-83.49-205.89-192-221.46zM496 384H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h480c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z"></path><title>'+lblBellTitle+'</title></svg>';
    return icon.replace(/\[SIZE\]/g,size);
}

function L(label) {
    return chrome.i18n.getMessage(label);
}


function DomCheckSetup() {
    var settingsMenu=$('#settings_nav');
    if (!settingsMenu) return;
    var optimizerMenu=$('#optimizer_nav');
    if (!optimizerMenu) return;

    settingsMenu.append(' <a class="list-group-item" id="optimizer_nav" style="cursor:pointer" >'+DrawIcon()+' Optimizer</a>')
}

function LoadSetupPage() {
    $('.framed-content').empty();
    var lang=L("lang");
    $.get(chrome.extension.getURL("_locales/" + lang + "/setup.html"), function (html) {
        $('.framed-content').append(html.replace("[EXTURL]", chrome.extension.getURL("")).replace("[ICON]",DrawIcon(30)));
        new doptSetup().Init();
    });
}

function GetObject(moduleName) {
    return $.grep(this.Modules, function(item){
        return item.name == moduleName;
      })[0];    
}

function Init(module) {
    moduleObj=new module();
    moduleObj.Init();
    this.Modules.push( moduleObj);
}

function LoadModules() {
    if (!IsPod()) return;
    $("head").append($("<link rel='stylesheet' href='" + chrome.extension.getURL("css/optimize.css") + "' type='text/css' media='screen' />"));

    Init(doptComments);
    Init(doptMessages);

    StartObservation();
    Log.Info('DiasporaOptimizer Modules loaded');
}

function IsPod() {
    var pod=localStorage["gpoPods"];
    if (!pod) return true;
    var pods=pod.split(',');
    var isPod=false;
    pods.forEach(function(singlePod) {
        if (window.location.host===singlePod) isPod=true;
    });
    return isPod;
}

function StartObservation() {
    diaspObserver.observe(document, {
        childList: true,
        subtree: true,
        characterData: false,
        attributes: false
    });
}

function Dom(node) {
    this.Modules.forEach(function(module) {
        module.Dom(node);
    }) 
}

var diaspObserver= new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {   
        oldUrl=window.location.href;
        if (mutation.type === "childList") {
            forEach.call(mutation.addedNodes, function (addedNode) {
                Dom(addedNode);
            });
        }
    });
});