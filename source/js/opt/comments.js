
var doptComments = function () {
    this.name = "comments";
    this.icons = [];
    this.myurl = undefined;
    
};

doptComments.prototype = {
    constructor: doptComments(),
    Init: function () {
     
        var comments = $('.comments');
        if (!comments) return;
        this.AddReply(comments);
        this.AddEvents();

    },

    AddEvents: function () {
        var self = this;
        $(document).on('click', '.optIcons b', function () {
            var emoticon = this.textContent;
            self.AddCommentTo(this.closest('.optComments'), emoticon, true);
        });

        $(document).on('click', '.optEmoticon', function (element) {
            $(this.parentElement).find(".optReactions").toggle();
        });

        $(document).on('click', '.optReply', function (element) {
            self.AddCommentTo(this.closest('.optComments'));
        });
    },

    AddCommentTo: function (parent, content, autosend) {
        var self=this;
        var container=$(parent).closest('.stream-element');
        var permalink=$(container).find('.permalink').attr('href');
        if (!container || container.length==0) {
            container=$(document);
            permalink=$(container).find('.post-time a').attr('href');
        }

        if (!container || container.length>1) {
            Log.Error("Wrong number of containers:"+container);
            return;
        }
        
        var commentUrl = permalink+ $(parent).data('commenturl');
        var author = $(parent).data('authorname');
        var authorHref = $(parent).data('authorurl');

        this.GetAuthorMarkDown(author, authorHref, permalink, function (authorObject) {
            var linkedContent = '';
            if (content) {
                linkedContent+=' ['+content+']('+commentUrl+')';
            } 
            if (authorObject && authorObject.handle) {
                linkedContent += ' @{' + authorObject.handle + '}'
            }

            $(container).find('.comment-box').trigger('click');
            $(container).find('.comment-box').focus();
            $(container).find('.comment-box').val(linkedContent);
            if (autosend) {
                $(container).find('input[type="submit"]').trigger('click');
            }
        });
    },

    ChildDom:function(node) {
        var self = this;
        if (node.childNodes && node.childNodes.length>0) {
            node.childNodes.forEach(function(childNode) {
                self.Dom(childNode);
            });
        }
    },

    Dom: function (node) {
        var self = this;
        
        if (!node.classList) return this.ChildDom(node);
        if (node.classList.length < 1) return this.ChildDom(node);
        if (node.classList[0] !== 'comment') return this.ChildDom(node);

        if (!this.myurl) this.myurl = $('#user-menu a').attr('href');
        this.GetCommentIcon('images/fa/comments/reply-solid.svg', function (replyicon) {
            self.GetCommentIcon('images/fa/comments/smile-plus-solid.svg', function (emoticon) {
                self.SingleComment(node, replyicon, emoticon);
            });
        });
    },
    GetCommentIcon(path, target) {
        if (icons[path]) {
            target(icons[path]);
            return;
        }
        $.get(chrome.extension.getURL(path), function (svg) {
            {
                icons[path] = new XMLSerializer().serializeToString(svg);
                target(icons[path]);
            }
        });
    },
    SingleComment: function (comment, replyIcon, emoticon) {
        var $author = $($(comment).find('.bd .author-name')[0]);
        var authorName = $author.text().trim();
        var authorUrl = $author.attr('href');
        if (authorUrl == this.myurl) return;
        var commentUrl = $(comment).find('.permalink_comment').attr('href');
        if (!commentUrl) return;
        commentUrl=commentUrl.substring(commentUrl.indexOf('#'));
        var icons = '<span class="optReactions"><span class="optIcons"><b>😊</b><b>😠</b><b>👍</b><b>👎</b><b>️❤️</b><b>️🦄</b><b>🖖</b><b>🎂</b></span></span>';
        var optReply = '<div class="optTooltip optAction optReply">' + replyIcon + '<span class="tooltiptext">' + L("reply") + '</span></div>';
        var optLike = '<div class="optTooltip optAction optEmoticon">' + emoticon + '<span class="tooltiptext">' + L("addReaction") + '</span>' + icons + '</div>';
        var optText = '<div class="optComments" data-authorname="' + authorName + '" data-commenturl="' + commentUrl + '" data-authorurl="' + authorUrl + '">' + DrawIcon(16) + optReply + optLike + '</div>';

        $author.after(optText);
    },

    GetAuthorMarkDown: function (name, url, permalink, target) {
        var found = false;

        if (!name) {
            target();
            return;
        }
        var completeUrl = window.location.origin+ permalink + '/mentionable.json?q=' + name;

        $.get(completeUrl, function (response) {
            if (response) {
                response.forEach(function (element) {
                    if (element.url === url) {
                        found = true;
                        target(element);
                    }
                });
            }
            if (!found) target();
        })
            .fail(function (err) {
                Log.Error("could not read userInformation. Will continue without. Status: " + err.status);
                target();
            });
    },


    AddReply: function (commentsDiv) {
        var iconAreas = $('.control-icons');
        if (iconAreas.length === 0) return;
        iconAreas.forEach(function (element) {
            SingleComment(element);
        });
    }
}
