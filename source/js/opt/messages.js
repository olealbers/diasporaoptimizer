var doptMessages = function () {
    this.name= "messages";
    this.reshareContent=' · '+DrawIcon(12)+'<a href="#" class="optReshareComment" rel="nofollow">'+L("shareWithComment")+'</a>';
};

doptMessages.prototype = {
    constructor: doptMessages(),
    Init: function () {
        if (!IsPod()) return;
        this.AddEvents();
    },
    AddEvents: function () {
        var self = this;
        $(document).on('click', '.optReshareComment', function () {

            var element=$(this).closest('.stream-element');
            var parent=element.find('.post-content');
            var photo=parent.find('.photo-attachments');
            var markdown=parent.find('.markdown-content');
            var photo=photo.html();
            var markdown=markdown.html();

            if (!photo) photo="";
            if (!markdown) markdown="";

            var html=photo+markdown;

            var und = new upndown();
            und.convert(html, function(err, markdown) {
                if(err) { console.log(err); }
                else {
                    self.GetAuthorMarkdown(element, function(author) {
                        markdown=author+markdown;
                        self.Post(markdown);
                    });                       
                } 
            });
        });
    },

    GetAuthorMarkdown:function(parent, target) {
        var author=parent.find('.author-name:first').text();
        var authorHref=parent.find('.author-name:first').attr('href');
        var permalink=parent.find('.permalink:first').attr('href');

        new doptComments().GetAuthorMarkDown(author, authorHref, permalink, function (authorObject) {
            
            var linkedContent = '['+L('sharedFrom')+']('+permalink+')';
            if (authorObject && authorObject.handle) {
                linkedContent += ' @{' + authorObject.handle + '}\n\n'
            }
            target(linkedContent);           
        });
    },

    Post:function(content) {
        var quote="";
        var i = j = 0;
        while ((j = content.indexOf('\n', i)) !== -1) {
            quote+="> "+content.substring(i, j)+'\n';
            i = j + 1;
        }
        quote+="> "+content.substring(i);

        quote="\n\n\n---\n"+quote;
      
        var editor=$('#status_message_text');
        
        $('#publisher-textarea-wrapper').addClass('active');
        editor.val(quote);

        $('.publisher.row.closed').removeClass('closed');
        
        var element=editor[0];
        editor.trigger('click');
        editor.focus();
    },

    Dom: function(node) {
        var self=this;
        $(node).find('.feedback .info').append(self.reshareContent);
    },
};